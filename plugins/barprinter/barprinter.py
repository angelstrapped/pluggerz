from . import bar


def entry_point():
    """Print the bar string from the bar module."""
    print(bar.bar)
