#!python3
# -*- coding: utf-8 -*-

import importlib
import pkgutil

import plugins

# Import entry points from every module in the plugins package.
plugin_entry_points = [
    importlib.import_module(name).entry_point
    for finder, name, ispkg
    in pkgutil.iter_modules(plugins.__path__, plugins.__name__ + ".")
]

if __name__ == "__main__":
    for entry_point in plugin_entry_points:
        entry_point()
